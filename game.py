def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over(board, winner):
        print_board(board)
        print("GAME OVER")
        print(winner, "has won")
        exit()


def is_row_winner(board,num):
    if board[num] == board[num+1] and board[num+1] == board[num+2]:
        return True

def is_column_winner(board,num):
    if board[num] == board[num + 3] and board[num + 3] == board[num + 6]:
        return True

def left_diagonal_is_winner(board):
    if board[0] == board[4] and board[4] == board[8]:
        return True

def right_diagonal_is_winner(board):
    if board[2] == board[4] and board[4] == board[6]:
        return True

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 0):
        game_over(board, board[0])
    elif is_row_winner(board, 3):
        game_over(board, board[3])
    elif is_row_winner(board, 6):
        game_over(board, board[6])
    elif is_column_winner(board, 0):
        game_over(board, board[0])
    elif is_column_winner(board, 1):
        game_over(board, board[1])
    elif is_column_winner(board, 2):
        game_over(board, board[2])
    elif left_diagonal_is_winner(board):
        game_over(board, board[0])
    elif right_diagonal_is_winner(board):
        game_over(board, board[0])

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
